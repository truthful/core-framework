<?php

namespace think\admin\service;

use think\admin\model\SysMerchant;
use think\admin\model\SysRelation;
use think\admin\model\SysRole;
use think\admin\Service;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

/**
 * 关系服务
 * Class RelationService
 * @package think\admin\service
 */
class RelationService extends Service
{
    /**
     * 查询用户与商户关系获取商户ID
     * @param int $userId
     * @return string
     */
    public static function getUserByMerchantId(int $userId): string
    {
        if (!$userId) {
            return '-1';
        }

        // 先查默认方式用户关联租户，取商户ID
        $merchantId = SysRelation::mk()->where(['object_id' => $userId, 'category' => 'SYS_USER_HAS_MERCHANT'])->value('target_id');

        // 如果不存在，则反查角色关联用户，取到角色ID
        if (!$merchantId) {
            $merchantId = SysRelation::mk()->where(['object_id' => $userId, 'category' => 'SYS_MERCHANT_HAS_USER'])->value('object_id');
        }

        // 如果仍不存在，则返回 -1
        if (!$merchantId) {
            return '-1';
        }

        return (string) $merchantId; // 将商户ID转换为字符串类型
    }

    /**
     * 查询用户与商户关系获取商户{ID,NAME}
     * @param int $userId
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function getUserByMerchant(int $userId): array
    {
        if (!$userId) {
            return [];
        }

        if (static::getUserByRoleType($userId) === 'GLOBAL') {
            // 如果是全局账号的显示全部,默认使用第一个商家作为默认数据
            $merchant = SysMerchant::mk()->where(['is_deleted' => 0, 'status' => 0])->select()->toArray();
            $merchant['self'] = $merchant ? $merchant[0]['merchantId'] : '';
        } else {
            if (AdminService::isMerchantAdmin()) {
                // 不是全局账号，且为商户管理员
                $merchants = SysRelation::mk()->where(['object_id' => $userId, 'category' => 'SYS_USER_HAS_MERCHANT'])->value('ext_json');
                if (!$merchants) {
                    // 没有用户与商户关联系统的
                    $merchantId = SysRelation::mk()->where(['target_id' => $userId, 'category' => 'SYS_MERCHANT_HAS_USER'])->value('object_id');
                    if ($merchantId) {
                        // 有商户关联用户信息的
                        $merchant = SysMerchant::mk()->where(['merchant_id' => $merchantId])->where(['is_deleted' => 0, 'status' => 0])->field('id,merchant_id as merchantId,name,info_type as infoType')->findOrEmpty();
                        $merchant['self'] = $merchant['merchantId'];
                    } else {
                        // 没有商户关联用户信息，也没有用户关联商户信息，则商户为空，则此账户来源有问题
                        $merchant = [];
                    }
                } else {
                    // 不是全局用户，不是商户管理员的
                    $merchant = json_decode($merchants, true);
                    $merchant['self'] = $merchant[0]['merchantId'];
                }
            } else {
                $merchant = [];
            }
        }
        return $merchant;
    }


    /**
     * 用户与角色类型的关系
     * @param int $userId
     * @return string
     */
    public static function getUserByRoleType(int $userId): string
    {
        $role = '';
        // 用户ID不存在反回空
        if (!$userId) return $role;
        if (AdminService::isSuper()) return 'GLOBAL';
        // 先查默认方式用户关联角色，取角色ID
        $relation = SysRelation::mk()->where(['object_id' => $userId, 'category' => 'SYS_USER_HAS_ROLE'])->column('target_id');
        // 如果不存在
        if (!$relation) {
            // 反查角色关联用户，取到角色ID
            $relation = SysRelation::mk()->where(['target_id' => $userId, 'category' => 'SYS_ROLE_HAS_USER'])->column('object_id');
            // 如果都不存在，则返回空值结束
            if (!$relation) return $role;
        }
        // 用户与角色关系存在，且关系不止一组的取到多个角色数组
        if ($relation && count($relation) > 1) {
            $roles = SysRole::mk()->whereIn('id', $relation)->where(['is_deleted' => 0, 'status' => 0])->column('category');
            // 查询多个角色数组中类型有没有GLOBAL全局类型有则默认都为全局类型，否则反回BIZ
            if (in_array('GLOBAL', $roles)) {
                return 'GLOBAL';
            } else {
                return 'BIZ';
            }
            // 用户与角色关系存在，只有一组的，则直接获取该角色的类型并返回
        } else {
            return SysRole::mk()->where(['id' => $relation[0]])->where(['is_deleted' => 0, 'status' => 0])->value('category');
        }
    }

    /**
     * 查询租户与商户关系获取商户列表
     * @param int $userId
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function getUserByMerchantList(int $userId): array
    {
        // 用户ID不存在返回空数组
        if (!$userId) return [];

        if (static::getUserByRoleType($userId) === 'GLOBAL') {
            // 如果是全局账号，显示全部商家，默认使用第一个商家作为默认数据
            $merchant = SysMerchant::mk()->where(['is_deleted' => 0, 'status' => 0])->select()->toArray();
        } else {
            // 如果不是全局账号，是BIZ账号
            if (AdminService::isMerchantAdmin()) {
                // 如果不是全局账号，且为商户管理员
                $merchants = SysRelation::mk()->where(['object_id' => $userId, 'category' => 'SYS_USER_HAS_MERCHANT'])->value('ext_json');
                if (!$merchants) {
                    // 如果没有关联商户信息，尝试反查商户关联用户，取到商户ID
                    $merchantId = SysRelation::mk()->where(['target_id' => $userId, 'category' => 'SYS_MERCHANT_HAS_USER'])->value('object_id');
                    if ($merchantId) {
                        $merchant = SysMerchant::mk()->where(['merchant_id' => $merchantId])->where(['is_deleted' => 0, 'status' => 0])->field('id,merchant_id as merchantId,name')->findOrEmpty()->toArray();
                    } else {
                        $merchant = [];
                    }
                } else {
                    $merchant = json_decode($merchants, true);
                }
            } else {
                $merchant = [];
            }
        }

        return $merchant;
    }

    /**
     * 用户是否属于商家管理员及商家标识
     * @param int $userId
     * @return array
     */
    public static function isMerchant(int $userId): array
    {
        $data = SysRelation::mk()->where(['object_id' => $userId, 'category' => 'SYS_USER_HAS_MERCHANT'])->value('target_id');
        if (!$data) {
            $data = SysRelation::mk()->where(['target_id' => $userId, 'category' => 'SYS_MERCHANT_HAS_USER'])->value('object_id');
        }
        return $data ? [true, $data] : [false, ''];
    }

}