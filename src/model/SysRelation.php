<?php


declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 系统用户资源关系表模型
 * Class SysRelation
 * @package think\admin\model
 */
class SysRelation extends Model
{
    /**
     * 字段转驼峰
     * @var bool
     */
    protected $convertNameToCamel = true;
    /**
     * 根据分类及对象ID获取目标数据（默认为target_id目标ID）
     * @param string $category
     * @param string $objectIds
     * @param string $field
     * @return array
     */
    public static function getTargetIds(string $category = '', string $objectIds = '', string $field = 'target_id'): array
    {
        return static::mk()->where(['category' => $category])->whereIn('object_id', $objectIds)->column($field);
    }


    /**
     * 根据分类及目标ID获取对象ID
     * @param string $category
     * @param string $targetIds
     * @return array
     */
    public static function getObjectIds(string $category = '', string $targetIds = ''): array
    {
        return static::mk()->where(['category' => $category])->whereIn('target_id', $targetIds)->column('object_id');
    }

    /**
     * 获取一条信息的详情
     * @param $id
     * @return array|mixed|SysRelation|\think\Model
     */
    public static function detail($id)
    {
        return static::mk()->where(['id' => $id])->findOrEmpty();
    }
}