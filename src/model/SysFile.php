<?php


declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 文件上传表模型
 * Class SysFile
 * @package think\admin\model
 */
class SysFile extends Model
{
    /**
     * 字段转驼峰
     * @var bool
     */
    protected $convertNameToCamel = true;

    // 追加的字段
    protected $append = [
        'downloadPath'     // 下载地址
    ];

    /**
     * 获取一条信息的详情
     * @param $id
     * @return array|mixed|SysFile|\think\Model
     */
    public static function detail($id)
    {
        return static::mk()->where(['id' => $id])->where(['is_deleted' => 0, 'status' => 0])->findOrEmpty();
    }

    /**
     * 生成下载地址 (downloadPath)
     * @param $value
     * @param $data
     * @return string
     */
    public function getDownloadPathAttr($value, $data): string
    {
        return sysuri('sys/file/download', ['id' => $data['id']], false, true);
    }
}