<?php


declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 全国省市区关系表模型
 * Class SysRegion
 * @package think\admin\model
 */
class SysRegion extends Model
{
    /**
     * 字段转驼峰
     * @var bool
     */
    protected $convertNameToCamel = true;

    /**
     * 获取一条信息的详情
     * @param $id
     * @return array|mixed|SysRegion|\think\Model
     */
    public static function detail($id)
    {
        return static::mk()->where(['id' => $id])->where(['is_deleted' => 0, 'status' => 0])->findOrEmpty();
    }
}