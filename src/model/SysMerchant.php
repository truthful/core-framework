<?php

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 商户表模型
 * Class SysMerchant
 * @package think\admin\model
 */
class SysMerchant extends Model
{
    /**
     * 字段转驼峰
     * @var bool
     */
    protected $convertNameToCamel = true;
    /**
     * 隐藏字段
     * @var string[]
     */
    protected $hidden = [
        'update_time', 'updated_by', 'sort'
    ];

    /**
     * 获取一条信息的详情
     * @param $id
     * @return array|mixed|SysMerchant|\think\Model
     */
    public static function detail($id)
    {
        return static::mk()->where(['id' => $id])->where(['is_deleted' => 0, 'status' => 0])->findOrEmpty();
    }
}