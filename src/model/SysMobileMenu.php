<?php


declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 移动菜单表模型
 * Class MobileMenu
 * @package think\admin\model
 */
class SysMobileMenu extends Model
{
    /**
     * 字段转驼峰
     * @var bool
     */
    protected $convertNameToCamel = true;

    /**
     * 获取一列数据
     * @param string $filed
     * @param string $filedValues
     * @param string $column
     * @return array
     */
    public static function getIds(string $filed, string $filedValues, string $column): array
    {
        return static::mk()->whereIn($filed, $filedValues)->column($column);
    }

    /**
     * 获取一条信息的详情
     * @param $id
     * @return array|mixed|MobileMenu|\think\Model
     */
    public static function detail($id)
    {
        return static::mk()->where(['id' => $id])->where(['is_deleted' => 0, 'status' => 0])->findOrEmpty();
    }
}