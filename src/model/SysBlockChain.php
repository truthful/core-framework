<?php

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;
use think\model\relation\HasOne;

/**
 * 上链纪录表模型
 * Class SysBlockChain
 * @package think\admin\model
 */
class SysBlockChain extends Model
{
    /**
     * 字段转驼峰
     * @var bool
     */
    protected $convertNameToCamel = true;

    protected $hidden = [
        'sort'
    ];

    /**
     * @param $id
     * @return SysBlockChain|array|mixed|\think\Model
     */
    public static function detail($id)
    {
        return static::mk()->where(['id' => $id])->where(['is_deleted' => 0, 'status' => 0])->findOrEmpty();
    }

    public function merchant(): HasOne
    {
        return $this->hasOne(SysMerchant::class, 'merchant_id', 'merchant_id')->where(['status' => 0])->bind(['merchant' => 'name']);
    }
}