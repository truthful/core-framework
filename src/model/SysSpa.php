<?php


declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;
use think\model\relation\HasOne;

/**
 * 单页表模型
 * Class SysModule
 * @package think\admin\model
 */
class SysSpa extends Model
{
    /**
     * 字段转驼峰
     * @var bool
     */
    protected $convertNameToCamel = true;

    protected $hidden = [
        'create_time','update_time','created_by','updated_by'
    ];

    /**
     * 获取一条信息的详情
     * @param $id
     * @return array|mixed|SysSpa|\think\Model
     */
    public static function detail($id)
    {
        return static::mk()->where(['id' => $id])->where(['is_deleted' => 0, 'status' => 0])->findOrEmpty();
    }

    /**
     * 关联模块（子系统）
     * @return HasOne
     */
    public function Modules(): HasOne
    {
        return $this->hasOne(SysModule::class, 'id', 'module')->where(['status' => 0, 'is_deleted' => 0]);
    }

}