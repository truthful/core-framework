<?php

namespace think\admin\model;

use app\wechat\service\PaymentService;
use think\admin\Model;
use think\model\relation\HasOne;

/**
 * 微信支付行为模型
 * @class SysWechatPaymentRecord
 * @package think\admin\Model
 */
class SysWechatPaymentRecord extends Model
{
    /**
     * 关联用户粉丝数据
     * @return \think\model\relation\HasOne
     */
    public function fans(): HasOne
    {
        return $this->hasOne(SysWechatFans::class, 'openid', 'openid');
    }

    /**
     * 绑定用户粉丝数据
     * @return \think\model\relation\HasOne
     */
    public function bindFans(): HasOne
    {
        return $this->fans()->bind([
            'fans_headimg'  => 'headimgurl',
            'fans_nickname' => 'nickname',
        ]);
    }

    /**
     * 格式化输出时间格式
     * @param mixed $value
     * @return string
     */
    public function getCreateTimeAttr($value): string
    {
        return $value ? format_datetime($value) : '';
    }

    /**
     * 格式化输出时间格式
     * @param mixed $value
     * @return string
     */
    public function getUpdateTimeAttr($value): string
    {
        return $value ? format_datetime($value) : '';
    }

    /**
     * 格式化输出时间格式
     * @param mixed $value
     * @return string
     */
    public function getPaymentTimeAttr($value): string
    {
        return $value ? format_datetime($value) : '';
    }

    public function toArray(): array
    {
        $data = parent::toArray();
        $data['type_name'] = PaymentService::tradeTypeNames[$data['type']] ?? $data['type'];
        return $data;
    }
}