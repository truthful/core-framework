<?php

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

/**
 * 标准代码模型
 * Class SysStandardCode
 * @package think\admin\model
 */
class SysStandardCode extends Model
{
    /**
     * 字段转驼峰
     * @var bool
     */
    protected $convertNameToCamel = true;

    /**
     * 获取多条信息
     * @param array $map
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function list(array $map): array
    {
        return static::mk()->where($map)->where(['is_deleted' => 0, 'status' => 0])->field('code,cn_value,en_value')->select()->toArray();
    }

    /**
     * 获取一条信息的详情
     * @param $id
     * @return array|mixed|SysStandardCode|\think\Model
     */
    public static function detail($id)
    {
        return static::mk()->where(['id' => $id])->where(['is_deleted' => 0, 'status' => 0])->findOrEmpty();
    }
}