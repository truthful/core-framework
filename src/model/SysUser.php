<?php

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;
use think\model\relation\HasMany;
use think\model\relation\HasOne;

/**
 * 后台用户表表模型
 * Class SysUser
 * @package think\admin\model
 */
class SysUser extends Model
{
    /**
     * 字段转驼峰
     * @var bool
     */
    protected $convertNameToCamel = true;

    /**
     * 获取一条信息的详情
     * @param $id
     * @return array|mixed|SysUser|\think\Model
     */
    public static function detail($id)
    {
        return static::mk()->where(['id' => $id])->where(['is_deleted' => 0, 'status' => 0])->findOrEmpty();
    }


    /**
     * 格式化输出时间
     * @param mixed $value
     * @return string
     */
    public function getUpdateTimeAttr($value): string
    {
        return format_datetime($value);
    }

    /**
     * 时间写入格式化
     * @param mixed $value
     * @return string
     */
    public function setUpdateTimeAttr($value): string
    {
        return is_string($value) ? str_replace(['年', '月', '日'], ['-', '-', ''], $value) : $value;
    }

    /**
     * 格式化输出手机号
     * @param $value
     * @return array|string|string[]
     */
    public function getPhoneAttr($value): array|string
    {
        return substr_replace($value, '****', 3, 4);
    }

    /**
     * 格式化输出邮箱
     * @param $value
     * @return string
     */
    public function getEmailAttr($value): string
    {
        $emailArray = explode('@', $value);
        $emailArray[0] = substr($emailArray[0], 0, 1) . str_repeat('*', strlen($emailArray[0]) - 2) . substr($emailArray[0], -1);
        return implode('@', $emailArray);
    }

}