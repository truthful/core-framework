<?php


declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 文件上传分组表模型
 * Class SysFileGroup
 * @package think\admin\model
 */
class SysFileGroup extends Model
{
    /**
     * 字段转驼峰
     * @var bool
     */
    protected $convertNameToCamel = true;

    /**
     * 获取一条信息的详情
     * @param $id
     * @return array|mixed|SysFileGroup|\think\Model
     */
    public static function detail($id)
    {
        return static::mk()->where(['id' => $id])->where(['is_deleted' => 0, 'status' => 0])->findOrEmpty();
    }
}