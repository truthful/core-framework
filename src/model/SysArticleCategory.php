<?php


declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 文章分类表模型
 * Class SysArticleCategory
 * @package think\admin\model
 */
class SysArticleCategory extends Model
{
    /**
     * 字段转驼峰
     * @var bool
     */
    protected $convertNameToCamel = true;

    /**
     * 获取一条信息的详情
     * @param $id
     * @return array|mixed|SysArticleCategory|\think\Model
     */
    public static function detail($id)
    {
        return static::mk()->where(['id' => $id])->where(['is_deleted' => 0, 'status' => 0])->findOrEmpty();
    }
}