<?php


declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;
use think\model\relation\hasMany;

/**
 * 模块表模型
 * Class SysModule
 * @package think\admin\model
 */
class SysModule extends Model
{
    /**
     * 字段转驼峰
     * @var bool
     */
    protected $convertNameToCamel = true;

    protected $hidden = [
        'create_time','update_time','created_by','updated_by'
    ];

    /**
     * 获取一条信息的详情
     * @param $id
     * @return array|mixed|SysModule|\think\Model
     */
    public static function detail($id)
    {
        return static::mk()->where(['id' => $id])->where(['is_deleted' => 0, 'status' => 0])->findOrEmpty();
    }

    /**
     * 模块关联的菜单
     * 一对多关系表
     * @return hasMany
     */
    public function menus(): hasMany
    {
        return $this->hasMany(SysMenu::class, 'module', 'id')->where(['is_deleted' => 0]);
    }

    /**
     * 模块关联手机端菜单
     * @return hasMany
     */
    public function mobileMenus(): hasMany
    {
        return $this->hasMany(SysMobileMenu::class, 'module', 'id')->where(['is_deleted' => 0]);
    }
}