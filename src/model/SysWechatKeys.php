<?php

namespace think\admin\model;

use think\admin\Model;

/**
 * 微信回复关键词模型
 * @class SysWechatKeys
 * @package think\admin\Model
 */
class SysWechatKeys extends Model
{
    /**
     * 格式化创建时间
     * @param string $value
     * @return string
     */
    public function getCreateAtAttr(string $value): string
    {
        return format_datetime($value);
    }
}