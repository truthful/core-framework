<?php

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 原产国（地区）代码表模型
 * 按GB/T 2659-2000
 * Class SysWorldCode
 * @package think\admin\model
 */
class SysWorldCode extends Model
{
    /**
     * 字段转驼峰
     * @var bool
     */
    protected $convertNameToCamel = true;

    /**
     * 获取一条信息的详情
     * @param $id
     * @return array|mixed|SysWorldCode|\think\Model
     */
    public static function detail($id)
    {
        return static::mk()->where(['id' => $id])->where(['is_deleted' => 0, 'status' => 0])->findOrEmpty();
    }
}