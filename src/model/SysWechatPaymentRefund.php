<?php

namespace think\admin\model;

use think\admin\Model;
use think\model\relation\HasOne;

/**
 * 微信支付退款模型
 * @class SysWechatPaymentRefund
 * @package think\admin\Model
 */
class SysWechatPaymentRefund extends Model
{
    /**
     * 关联支付订单
     * @return \think\model\relation\HasOne
     */
    public function record(): HasOne
    {
        return $this->hasOne(SysWechatPaymentRecord::class, 'code', 'record_code')->with('bindfans');
    }

    /**
     * 格式化输出时间格式
     * @param mixed $value
     * @return string
     */
    public function getCreateTimeAttr($value): string
    {
        return $value ? format_datetime($value) : '';
    }

    /**
     * 格式化输出时间格式
     * @param mixed $value
     * @return string
     */
    public function getUpdateTimeAttr($value): string
    {
        return $value ? format_datetime($value) : '';
    }

    /**
     * 格式化输出时间格式
     * @param mixed $value
     * @return string
     */
    public function getRefundTimeAttr($value): string
    {
        return $value ? format_datetime($value) : '';
    }
}