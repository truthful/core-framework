<?php
declare (strict_types=1);

namespace think\admin\helper;

use think\admin\Helper;
use think\admin\service\AdminService;
use think\db\BaseQuery;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\db\Query;
use think\exception\HttpResponseException;
use think\Model;

/**
 * 列表处理管理器
 * Class PageHelper
 * @package think\admin\helper
 */
class PageHelper extends Helper
{
    /**
     * 逻辑器初始化
     * @param Model|BaseQuery|string $dbQuery
     * @param boolean $page 是否启用分页
     * @param boolean $display 是否渲染模板
     * @param boolean|integer $total 集合分页记录数
     * @param integer $limit 集合每页记录数
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function init($dbQuery, bool $page = true, bool $display = true, $total = false, int $limit = 0): array
    {
        $query = $this->autoSortQuery($dbQuery);

        if ($page !== false) {
            $get = $this->app->request->get();
            $limits = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200];
            if ($limit <= 1) {
                $limit = $get['limit'] ?? $this->app->cookie->get('limit', 10);
                if (in_array($limit, $limits) && ($get['not_cache_limit'] ?? 0) < 1) {
                    $this->app->cookie->set('limit', ($limit = intval($limit >= 5 ? $limit : 10)) . '');
                }
            }
            // 生成分页数据
            $config = ['list_rows' => $limit, 'query' => $get];
            if (is_numeric($page)) $config['page'] = $page;
            $data = ($paginate = $query->paginate($config, $this->getCount($query, $total)))->toArray();
            $result = ['page' => ['limit' => $data['per_page'], 'total' => $data['total'], 'pages' => $data['last_page'], 'current' => $data['current_page']], 'list' => $data['data']];
            //xss过滤处理
            static::xssFilter($result['list']);
            //某些字段后处理（图片、重要信息脱敏）
            $this->fieldAfterMk($query, $result['list']);
        } else {
            $result['list'] = $query->select()->toArray();
            //某些字段后处理（图片、重要信息脱敏）
            $this->fieldAfterMk($query, $result['list']);
            $result = ['list' => $result['list']];
        }
        if (false !== $this->class->callback('_page_filter', $result['list'], $result) && $display) {
            $this->class->success('JSON数据获取成功！', $result);
        }
        return $result;
    }

    /**
     * 组件 ant design.Table 处理
     * @param BaseQuery|Model|string $dbQuery
     * @param string $template
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function antTable($dbQuery, string $template = ''): array
    {
        if ($this->output === 'get.json') {
            $get = $this->app->request->get();
            $query = static::buildQuery($dbQuery);
            // 根据参数排序
            if (isset($get['_field_']) && isset($get['_order_'])) {
                $dbQuery->order("{$get['_field_']} {$get['_order_']}");
            }
            return PageHelper::instance()->init($query);
        }
        if ($this->output === 'get.ant.table') {
            $get = $this->app->request->get();
            $query = $this->autoSortQuery($dbQuery);
            // 根据参数排序
            if (isset($get['_field_']) && isset($get['_order_'])) {
                $query->order("{$get['_field_']} {$get['_order_']}");
            }
            // 数据分页处理
            if (empty($get['page']) || empty($get['limit'])) {
                $data = $query->select()->toArray();
                $result = ['msg' => '', 'code' => 0, 'count' => count($data), 'data' => $data];
            } else {
                $cfg = ['list_rows' => $get['limit'], 'query' => $get];
                $data = $query->paginate($cfg, static::getCount($query))->toArray();
                $result = ['msg' => '', 'code' => 0, 'count' => $data['total'], 'data' => $data['data']];
            }
            if (false !== $this->class->callback('_page_filter', $result['data'], $result)) {
                //某些字段后处理（图片、重要信息脱敏）
                $this->fieldAfterMk($query, $result['data']);
                static::xssFilter($result['data']);
                throw new HttpResponseException(json($result));
            } else {
                return $result;
            }
        } else {
            $this->class->fetch($template);
            return [];
        }
    }

    /**
     * 绑定排序并返回操作对象
     * @param Model|BaseQuery|string $dbQuery
     * @return Query
     * @throws DbException
     */
    public function autoSortQuery($dbQuery): Query
    {
        $query = static::buildQuery($dbQuery);
        $get = $this->app->request->get();
        if ($this->app->request->isGet() && isset($get['_field_']) && isset($get['_order_'])) {
            $_field_ = str_replace('end', '', strtolower(trim(preg_replace("/[A-Z]/", "_\\0", $get['_field_']), '_')));
            $_order_ = str_replace('end', '', $get['_order_']);
            // 根据参数排序
            $query->order("{$_field_} {$_order_}");
        }
        if ($this->app->request->isPost() && $this->app->request->post('action') === 'sort') {
            AdminService::isLogin() or $this->class->error('请重新登录！');
            if (method_exists($query, 'getTableFields') && in_array('sort', $query->getTableFields())) {
                if ($this->app->request->has($pk = $query->getPk() ?: 'id', 'post')) {
                    $map = [$pk => $this->app->request->post($pk, 0)];
                    $data = ['sort' => intval($this->app->request->post('sort', 0))];
                    if ($query->newQuery()->where($map)->update($data) !== false) {
                        $this->class->success('列表排序成功！', '');
                    }
                }
            }
            $this->class->error('列表排序失败！');
        }
        return $query;
    }

    /**
     * 查询对象数量统计
     * @param BaseQuery|Query $query
     * @param boolean|integer $total
     * @return integer|boolean|string
     */
    private static function getCount($query, $total = false)
    {
        if ($total === true || is_numeric($total)) return $total;
        [$query, $options] = [clone $query, $query->getOptions()];
        if (empty($options['union'])) return $query->count();
        $table = [$query->buildSql() => '_union_count_'];
        return $query->newQuery()->table($table)->count();
    }

    /**
     * 输出 XSS 过滤处理
     * @param array $items
     */
    private static function xssFilter(array &$items)
    {
        foreach ($items as &$item) if (is_array($item)) {
            static::xssFilter($item);
        } elseif (is_string($item)) {
            $item = htmlspecialchars($item, ENT_QUOTES);
        }
    }

    /**
     * 特殊字段的后处理（图片类、重要信息脱敏）
     * @param $query
     * @param array $items
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    private function fieldAfterMk($query, array &$items)
    {
        if (method_exists($query, 'getTableFields')) {
            $fields = $query->getTableFields();
            if (count($items) > 0) {
                $itemsFields = array_keys($items[0]);
                //数据中含有图片ID的字段通过键名判断获取相应图片URL(图片数组命名方式：字段名+Arr,例字段logo,则对应为logoArr)
                foreach ($itemsFields as $field) {
                    if (in_array(static::toCamelCase($field), ['file', 'image', 'shopPic', 'materialImage', 'productImage', 'productLabelImage', 'productImageWithSupportingElements', 'productFormulationStatement', 'planogram', 'nutritionFactLabel', 'ifu', 'certification', 'barcode', 'degreeImage360', 'imageId', 'pic', 'img', 'logo', 'brandLogo', 'certificate', 'license', 'businessLicense', 'qualificationDocuments'])) {
                        foreach ($items as &$v) {
                            if ($v[$field]) {
                                $ids = explode(',', $v[$field]);
                                $v[$field . 'Arr'] = $this->autoSortQuery('sys_file')->whereIn('id', $ids)->field('id,name,storage_path as storagePath,suffix,size_info')->select()->toArray();
                                if (count($v[$field . 'Arr']) == 1) $v[$field . '_url'] = $v[$field . 'Arr'][0]['storagePath'];

                            }
                        }
                    }
                }
            }
            //特殊字段的后期处理方式，读取表字段
            /*foreach ($fields as $field) {
                //手机号，电话号脱敏
                if (in_array($this->toCamelCase($field), ['phone', 'contact_phone', 'mobile', 'tel'])) {
                    foreach ($items as &$v) {
                        if ($v[$field]) {
                            $v[$field] = desensitize($v[$field], 3, 4);
                        }
                    }
                }
                //邮箱脱敏
                if (in_array($field, ['email', 'contact_mail', 'mail'])) {
                    foreach ($items as &$v) {
                        if ($v[$field]) {
                            $newfield = explode('@', $v[$field]);
                            $len = strlen($newfield[1]) + 1;
                            $v[$field] = desensitize($v[$field], 1, $len);
                        }
                    }
                }
                //姓名，真名脱敏
                if (in_array($field, ['leader', 'realname'])) {
                    foreach ($items as &$v) {
                        if ($v[$field]) {
                            $v[$field] = desensitize($v[$field], 1, 1);
                        }
                    }
                }
            }*/
        }
    }

    /**
     * 下划线命名到驼峰命名
     * @param $str
     * @return mixed|string
     */
    private static function toCamelCase($str)
    {
        $array = explode('_', $str);
        $result = $array[0];
        $len = count($array);
        if ($len > 1) {
            for ($i = 1; $i < $len; $i++) {
                $result .= ucfirst($array[$i]);
            }
        }
        return $result;
    }

}