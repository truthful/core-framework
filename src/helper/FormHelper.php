<?php

declare (strict_types=1);

namespace think\admin\helper;

use think\admin\Exception;
use think\admin\Helper;
use think\admin\service\AdminService;
use think\admin\service\SystemService;
use think\db\BaseQuery;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\Model;

/**
 * 表单视图管理器
 * @class FormHelper
 * @package think\admin\helper
 */
class FormHelper extends Helper
{

    /**
     * 逻辑器初始化
     * @param BaseQuery|Model|string $dbQuery
     * @param string $template 视图模板名称
     * @param string $field 指定数据主键
     * @param mixed $where 限定更新条件
     * @param array $edata 表单扩展数据
     * @return void|array|boolean
     * @throws Exception
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function init($dbQuery, string $template = '', string $field = '', $where = [], array $edata = [])
    {
        $query = static::buildQuery($dbQuery);
        $field = $field ?: ($query->getPk() ?: 'id');
        $value = $edata[$field] ?? input($field);
        if ($this->app->request->isGet()) {
            if ($value !== null) {
                $exist = $query->where([$field => $value])->where($where)->find();
                if ($exist instanceof Model) $exist = $exist->toArray();
                $edata = array_merge($edata, $exist ?: []);
            }
            if (false !== $this->class->callback('_form_filter', $edata)) {
                $this->class->fetch($template, ['vo' => $edata]);
            } else {
                return $edata;
            }
        }
        if ($this->app->request->isPost()) {
            $edata = array_merge($this->app->request->post(), $edata);
            //增加发布员，修改员ID写入对应表
            if($this->app->request->post($field)){
                $edata = array_merge($edata,['updated_by' => AdminService::getUserId()]);
            }else{
                $edata = array_merge($edata,['created_by' => AdminService::getUserId()]);
            }
            if (false !== $this->class->callback('_form_filter', $edata, $where)) {
                $result = SystemService::save($query, $edata, $field, $where) !== false;
                if (false !== $this->class->callback('_form_result', $result, $edata)) {
                    if ($result !== false) {
                        $this->class->success('数据保存成功！');
                    } else {
                        $this->class->error('数据保存失败！');
                    }
                }
                return $result;
            }
        }
    }
}